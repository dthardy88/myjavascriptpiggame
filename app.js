/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result gets added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost AND if he rolls a 6 his whole score gets reset to 0. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, diceOne, gamePlaying, winningScore;
var diceRolls = [];

// Initialise game
init();

// Remove the dice
document.querySelector(".dice-1").style.display = "none";
document.querySelector(".dice-2").style.display = "none";

// Set the on click event
document.querySelector(".btn-roll").addEventListener('click', function()
{
  if(gamePlaying)
  {
  // Create the dice
  diceOne = Math.floor(Math.random() * 6) + 1;
  diceTwo = Math.floor(Math.random() * 6) + 1;
  // 2. Display the result
  var diceDOMOne = document.querySelector('.dice-1');
  var diceDOMTwo = document.querySelector('.dice-2');

  diceDOMOne.style.display = 'block';
  diceDOMOne.src = 'dice-' + diceOne + '.png';

  diceDOMTwo.style.display = 'block';
  diceDOMTwo.src = 'dice-' + diceTwo + '.png';

  if(diceOne !== 1 && diceTwo !== 1)
  {
    // Add score
    roundScore += (diceOne + diceTwo);
    document.querySelector('#current-' + activePlayer).textContent = roundScore;
  }
  else {
    document.querySelector(".btn-continue-1").style = "display:block;"
    document.querySelector('.btn-roll').style = "display:none;";
    document.querySelector('.btn-hold').style = "display:none;";
  }

  // Save the previous roll
  diceRolls.push(diceOne);
  diceRolls.push(diceTwo);

  if(diceRolls.length > 2)
  {
    diceRolls.shift(diceRolls);
    diceRolls.shift(diceRolls);
  }

  // If you roll two 6's in a row
  if(diceRolls[0] == 6 && diceRolls[1] == 6 || diceRolls[2] == 6 && diceRolls[3] == 6) {
    document.getElementById('score-' + activePlayer).textContent = 0;
    scores[activePlayer] = 0;
    roundScore = 0;

    document.querySelector(".btn-continue-2").style = "display:block;";
    document.querySelector('.btn-roll').style = "display:none;";
    document.querySelector('.btn-hold').style = "display:none;";
  }
}

});
// The continue button for when you roll two 6's
document.querySelector(".btn-continue-2").addEventListener('click', function()
{
  nextPlayer();
  document.querySelector(".btn-continue-2").style = "display:none";
});
// The continue button for when you roll a 1
document.querySelector(".btn-continue-1").addEventListener('click', function()
{
  document.querySelector(".btn-continue-1").style = "display:none";
  nextPlayer();
});

document.querySelector('.btn-hold').addEventListener('click', function()
{
  if(gamePlaying)
  {
  // Add current score to the global score
  scores[activePlayer] += roundScore;
  // Update the UI
  document.getElementById('score-' + activePlayer).textContent = scores[activePlayer];
  // Check if player won the game
  if(scores[activePlayer] >= winningScore) {
    // Active player has won the game
    document.getElementById('name-' + activePlayer).textContent = "Winner";
    document.querySelector('.dice-1').style.display = 'none';
    document.querySelector('.dice-2').style.display = 'none';
    document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
    document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
    gamePlaying = false;
  }
  else {
    // Next players go
    nextPlayer();
  }
  }
});

function nextPlayer()
{
  // Next player
  activePlayer == 0 ? activePlayer = 1 : activePlayer = 0;
  roundScore = 0;

  document.querySelector('.btn-roll').style = "display:block;";
  document.querySelector('.btn-hold').style = "display:block;";

  document.getElementById('current-0').textContent = '0';
  document.getElementById('current-1').textContent = '0';

  document.querySelector('.player-0-panel').classList.toggle('active');
  document.querySelector('.player-1-panel').classList.toggle('active');

  document.querySelector(".dice-1").style.display = "none";
  document.querySelector(".dice-2").style.display = "none";
}

document.querySelector('.btn-new').addEventListener('click', init);

function init()
{
  scores = [0,0];
  activePlayer = 0;
  roundScore = 0;
  gamePlaying = true;
  winningScore = prompt('Please enter the winning score', 100);

  // Set all scores to 0
  document.getElementById('score-0').textContent = '0';
  document.getElementById('score-1').textContent = '0';
  document.getElementById('current-0').textContent = '0';
  document.getElementById('current-1').textContent = '0';
  document.getElementById('name-0').textContent = 'Player 1';
  document.getElementById('name-1').textContent = 'Player 2';
  document.querySelector('.player-0-panel').classList.remove('winner');
  document.querySelector('.player-1-panel').classList.remove('winner');
  document.querySelector('.player-0-panel').classList.remove('active');
  document.querySelector('.player-1-panel').classList.remove('active');
  document.querySelector('.player-0-panel').classList.add('active');
  document.querySelector('.btn-continue-1').style = "display:none;";
  document.querySelector('.btn-continue-2').style = "display:none;";
  document.querySelector('.btn-roll').style = "display:block;";
  document.querySelector('.btn-hold').style = "display:block;";
  document.querySelector(".dice-1").style.display = "none";
  document.querySelector(".dice-2").style.display = "none";
}
